{-# LANGUAGE OverloadedStrings #-} 

module RunServer 
  (
    runServerFromComandline
  ) where

import Control.Monad
import Data.Text.Format as F
import Network (PortNumber)
import System.Console.GetOpt
import System.Directory
import System.Environment
import System.Exit
import System.IO
import System.Posix.Daemonize (daemonize)
import System.Posix.Process (getProcessID)

import qualified NewEdenRouting

import Thrift
import Thrift.Protocol.Binary
import Thrift.Transport
import Thrift.Server

data Options = Options  { optVerbose   :: Bool
                        , optDatabase  :: String
                        , optPidfile   :: String
                        , optPort      :: PortNumber
                        , optDaemonize :: Bool
                        }

startOptions :: Options
startOptions = Options  { optVerbose   = False
                        , optDatabase  = "neweden-latest.sqlite"
                        , optPidfile   = "/var/run/cynoup-server.pid"
                        , optPort      = 9090
                        , optDaemonize = False
                        }

options :: [ OptDescr (Options -> IO Options) ]
options =
    [ Option "f" ["file"]
        (ReqArg
            (\arg opt -> return opt { optDatabase = arg })
            "FILE")
        "Path to static Sqlite database of New Eden"

    , Option [] ["pidfile"]
        (ReqArg
            (\arg opt -> return opt { optPidfile = arg })
            "pidfile")
        "The pidfile"

    , Option "d" ["daemonize"]
        (NoArg
            (\opt -> return opt { optDaemonize = True }))
        "Daemonize the service"

    , Option "p" ["port"]
        (ReqArg
            (\arg opt -> return opt { optPort = fromInteger (read arg) })
            "PORT")
        "Port to run on (default: 9090)"

    , Option "v" ["verbose"]
        (NoArg
            (\opt -> return opt { optVerbose = True }))
        "Enable verbose messages"

    , Option "h" ["help"]
        (NoArg
            (\_ -> do
                prg <- getProgName
                hPutStrLn stderr (usageInfo prg options)
                exitWith ExitSuccess))
        "Show help"
    ]

--runServer :: IO ()
runServerFromComandline serverProcess = do
    args <- getArgs

    -- Parse options, getting a list of option actions
    let (actions, nonOptions, errors) = getOpt RequireOrder options args

    opts <- foldl (>>=) (return startOptions) actions

    let Options { optVerbose = verbose
                , optDatabase = dbPath
                , optPort = port
                , optDaemonize = shouldDaemonize
                , optPidfile = pidfile
                } = opts

    -- Validate our options
    exists <- doesFileExist dbPath
    unless exists (do
        hprint stderr "Database path '{}' does not exist\n" (Only dbPath)
        exitWith (ExitFailure 1))

    if shouldDaemonize then
        daemonize (handler dbPath port pidfile)
    else
        serverProcess dbPath port

    where
        handler dbPath port pidfile = do
            pid <- getProcessID
            writeFile pidfile (show pid)
            serverProcess dbPath port
